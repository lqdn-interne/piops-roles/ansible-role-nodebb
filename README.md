# Ansible role NodeBB

Ansible role to install and update nodeBB

Requirements
------------

Roles :

```
- name: geerlingguy.certbot
- name: geerlingguy.nginx
- name: geerlingguy.nodejs
 ```

Collections :

```
- name: community.mongodb
```

You can use the `requirements.yml` file as model.

Role Variables
--------------

``nodebb_required_nodejs_version: "v18."``

The required nodeJS version

``nodebb_required_npm_version: "8."``

The required npm version

``nodebb_mongodb_version: "6."``

The required mongoDB version

Dependencies
------------

- https://galaxy.ansible.com/community/mongodb

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - ansible-role-nodebb
      vars:
        - nodebb_url : "forum.example.net"

License
-------

AGPLv3

Author Information
------------------

Written by Nono for La Quadrature Du Net